#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="taurusgui-magnetpanel",
    version="1.0.2",
    description="QT widget for controlling a magnet circuit.",
    author="Johan Forsberg",
    author_email="johan.forsberg@maxlab.lu.se",
    license="GPLv3",
    url="http://www.maxlab.lu.se",
    include_package_data=True,
    packages=find_packages(),
    install_requires=["pytango", "taurus"],
    package_data={"": ["*.ui"]},
    entry_points={"console_scripts": [
        "cttrimcoil = magnetpanel:trimcoil_main",
        "ctmagnet = magnetpanel:magnet_main"
    ]}
)
